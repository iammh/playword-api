<?php
include_once("../models/concepts.php");
header('Access-Control-Allow-Origin: *');
$concepts =  new Concepts();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
    $_POST = json_decode(file_get_contents('php://input'), true);
  }

if($_POST) {
header('Content-Type: application/json');
  if(isset($_POST['value']) && isset($_POST['type'])) {
    $val = $concepts->insert($_POST['value'],$_POST['type']);

    if($val == true) {
      $a['success'] = true;
      echo json_encode($a);
    }
    else {
      $a['error'] = $val;
      echo json_encode($a);
    }
  }
  else {
    $a[error] = "Can not be empty";
    echo json_encode($a);
  }
}
else {
  header('Content-Type: application/json');
  if(isset($_GET['id'])) {
    $id = $_GET['id'];
    echo $concepts->get($id);
  }
  else {
    echo $concepts->getAll();
  }
}
header('Content-Type: application/json');
$x['message'] = 'on concepts';
