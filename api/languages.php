<?php
include_once("../models/languages.php");
$languages =  new Languages();
header('Content-Type: application/json');
if($_POST) {

  if(isset($_POST['name'])) {
    $val = $languages->insert($_POST['name']);

    if($val != true) {
      $a['error'] = $val;
      echo json_encode($a);
    }
    else {
      $a['success'] = true;
      echo json_encode($a);
    }
  }
}
else {

  if(isset($_GET['id'])) {
    $id = $_GET['id'];
    echo $languages->get($id);
  }
  else {
    echo $languages->getAll();
  }
}
