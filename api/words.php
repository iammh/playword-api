<?php
header('Access-Control-Allow-Origin: *');
include_once("../models/words.php");
$words =  new Words();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST)) {
    $_POST = json_decode(file_get_contents('php://input'), true);
  }
if($_POST) {

  if(isset($_POST['title'])) {
    $data = $_POST['title'];
    if($data !="") {
      $val = $words->insert($_POST['title']);
header('Content-Type: application/json');
      if($val != true) {
        $a['error'] = $val;
        echo json_encode($a);
      }
      else {
        $a['success'] = true;
        echo json_encode($a);
      }
    }else {

      $a['error'] = 'Can not be Empty';
      echo json_encode($a);
    }

  }
}
else {

  header('Content-Type: application/json');
  if(isset($_GET['id'])) {
    $id = $_GET['id'];
    echo $words->get($id);
  }
  else if(isset($_GET['count'])) {
    $id = $_GET['count'];
    echo $words->count();
  }
  else {
    echo $words->getAll();
  }
}
