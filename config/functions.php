<?php
function ResultSetTOJson($resultset)
{
  if($resultset->num_rows > 0) {
    $output = [];
    while ($rows = $resultset->fetch_assoc()) {
      array_push($output, $rows);
    }
    return json_encode($output);
  }
  $error['error'] = "No data found";
  return json_encode($error);
}

function ResultTOJson($result)
{
  if($result->num_rows == 1) {
    $output = $result->fetch_assoc();
    return json_encode($output);
  }
  $e['data'] = 0;
  return json_encode($e);
}
