<?php
include ("config.php");



class Database {

    public $connection = null;

    private $DB_NAME = 'playword';
    private $DB_HOSTNAME = 'localhost';
    private $DB_USER = 'root';
    private $DB_PASSWORD = '';
    private $DB_PORT = '';


    function __construct() {

       // echo "Database object created<br>";

        $this->connection = new mysqli($this->DB_HOSTNAME, $this->DB_USER, $this->DB_PASSWORD, $this->DB_NAME);
        if ($this->connection->connect_error) {
            echo "Connection error";
            die("error in connnection<br>");
        }
        //echo "Connected<br>";
    }


    public function get () {
        return $this->connection;
    }

    public function query ($sql) {

        $data = $this->connection->query($sql);
        if(!$data) {
           echo $this->connection->error;
        }
        return $data;
        /*
        if($this->connection->query($sql)) {
            //echo "Inserted<br>";
        }
        else {
            //echo "Error <br>";
        }*/

    }
    public function close () {
        $this->connection->close();
    }

}
