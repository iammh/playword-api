<?php
include_once("../config/database.php");
include_once("../config/functions.php");
/**
 * Languages
 */
class Languages extends Database
{
  private $MODEL_NAME = NULL;
  function __construct()
  {
    parent::__construct();
    $this->MODEL_NAME = 'playword_languages';
  }

  public function insert($title='')
  {
    $sql = "SELECT * FROM $this->MODEL_NAME WHERE value='$title'";
    if($this->query($sql)->num_rows >0) {
      return false;
    }
    $sql = "INSERT INTO `$this->MODEL_NAME` (`_id`, `value`) VALUES (NULL, '$title');";
    return $this->query($sql);
  }

  public function get($id=1)
  {
    $sql = "SELECT * FROM $this->MODEL_NAME WHERE _id=$id";
    return ResultToJson($this->query($sql));
  }

  public function getAll()
  {
    $sql = "SELECT * FROM $this->MODEL_NAME";
    return ResultSetToJson($this->query($sql));
  }
}
