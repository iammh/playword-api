<?php

include_once "../config/database.php";


function GET_ROWS ($resultset) {

    if($resultset->num_rows == 1 ) {

       $row = $resultset->fetch_assoc();
       return json_encode($row);

    }
    else if($resultset->num_rows > 0) {


        $output = [];
        while($rows = $resultset->fetch_assoc()) {

            //$data['email'] = $rows['email'];
            //$data['username'] = $rows['username'];
            //$data['password'] = $rows['password'];

            array_push($output,$rows);

        }

        return json_encode($output);
    }
    else {

          $error['error'] = false;
           $error['data'] = "No data found";
           return json_encode($error);
        }

}

class Users extends Database{
    public function __construct () {
        parent::__construct();
        //echo "Users model Created<br>";
    }

    public function insert ($user) {
        $email = $user['email'];
        $username = $user['username'];
        $password = $user['password'];

        $sql = "INSERT INTO `users` (`serial`, `email`, `password`, `username`) VALUES (NULL, '$email', '$password', '$username');";

        echo json_encode($this->query($sql));
    }

    public function getUsers () {

         $sql = "Select * from users";
         //print_r ($this->query($sql));
         echo GET_ROWS($this->query($sql));

    }

    public function getUser ($id) {
        $sql = "Select * from users where serial=".$id;
             //print_r ($this->query($sql));
        echo GET_ROWS($this->query($sql));
    }
}
