<?php
include_once("../config/database.php");
include_once("../config/functions.php");
/**
 * Concepts
 * For multiple concepts
 * INSERT INTO `playword_word_types` (`_id`, `name`) VALUES (NULL, 'Verb'), (NULL, 'Noun');
 */
class Concepts extends Database
{
  private $MODEL_NAME = NULL;
  function __construct()
  {
    parent::__construct();
    $this->MODEL_NAME = 'playword_concepts';
  }

  public function insert($value, $type)
  {

    $sql = "SELECT * FROM $this->MODEL_NAME WHERE value='$value'";
    if($this->query($sql)->num_rows >0) {
      return false;
    }
    $sql = "INSERT INTO `playword_concepts` (`_id`, `value`, `type`) VALUES (NULL, '$value', (SELECT `_id` FROM `playword_word_types` WHERE `name`='$type'));";
    return $this->query($sql);
  }

  public function get($id=1)
  {
    $sql = "SELECT * FROM $this->MODEL_NAME WHERE _id=$id";
    return ResultToJson($this->query($sql));
  }

  public function getAll()
  {
    $sql = "SELECT * FROM $this->MODEL_NAME";
    return ResultSetToJson($this->query($sql));
  }

  public function mapConcepts($word, $concept)
  {
    
  }
}
